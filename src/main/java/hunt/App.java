package hunt;

import com.google.common.cache.*;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.FluentStringsMap;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
public class App {
    static LoadingCache<String, String> cache = null;

    public static void main(String[] args) throws ExecutionException {

        cache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .expireAfterWrite(10, TimeUnit.SECONDS)
                .removalListener(new RemovalListener<Object, Object>() {
                    @Override
                    public void onRemoval(RemovalNotification<Object, Object> notification) {
                        System.err.println("removed" + notification.getKey());
                    }
                })
                .build(
                        new CacheLoader<String, String>() {
                            public String load(String key) throws Exception {

                                if (key == "cat") {
                                    throw new UnsupportedOperationException("I don't like cats...");
                                }

                                if (key == "snake") {
                                    //snake is dangerous, we don't have one... lets return null
                                    return null;
                                }
                                System.err.println("fetching :" + key);
                                ListenableFuture<Response> execute;
                                FluentStringsMap fs = new FluentStringsMap();
                                fs.add("q", key);
                                fs.add("search_location", "United Kingdom");
                                fs.add("category", "all");
                                fs.add("search_scope", "title");
                                execute = new AsyncHttpClient().preparePost("http://www.gumtree.com/search").setParameters(fs).execute();
                                Document doc = Jsoup.parse(execute.get().getResponseBody());
                                return doc.select("h3.summary").text();
                            }
                        });
        System.out.println(cached("dog"));
        System.out.println(cached("dog"));
        System.out.println(cached("dog"));
        System.out.println(cached("cat"));
        System.out.println(cached("snake"));
        System.out.println(cached("dog"));
        System.out.println(cached("dog"));
        System.out.println(cached("cat"));
        System.out.println(cached("dog"));
        System.out.println(cached("dog"));
        System.out.println(cached("dog"));
        System.out.println(cached("dog"));
    }

    private static String cached(String key) {
        String s = null;
        try {
            s = cache.get(key);
        } catch (Throwable t) {
            System.out.println("error:" + t);
        }
        if (s == null) {
            System.out.println("miss for " + key);
        }
        return s;
    }
}
